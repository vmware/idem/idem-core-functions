import asyncio
import sys
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    for dyne in ["docs", "exec", "states", "tests", "tool"]:
        hub.pop.sub.add(dyne_name=dyne)

    yield hub


async def _setup_hub(hub):
    # Set up the hub before each function here
    ...


async def _teardown_hub(hub):
    # Clean up the hub after each function here
    ...


@pytest.fixture(scope="function", autouse=True)
async def function_hub_wrapper(hub):
    await _setup_hub(hub)
    yield
    await _teardown_hub(hub)


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    print(f"code_dir:{code_dir}")
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem"], "idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}

        yield hub
