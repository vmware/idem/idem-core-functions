import pytest


@pytest.mark.asyncio
async def test_string_replace(hub):
    result = await hub.exec.core.string.replace(
        data="test_data", to_replace="test", replace_with="test_updated"
    )
    assert result["result"], result["ret"]
    assert "test_updated_data" == result["ret"]["data"]


@pytest.mark.asyncio
async def test_string_replace_with_no_matching_substring(hub):
    result = await hub.exec.core.string.replace(
        data="test_data", to_replace="test-update", replace_with="test_updated"
    )
    assert result["result"], result["ret"]
    assert "test_data" == result["ret"]["data"]


@pytest.mark.asyncio
async def test_empty_string_replace(hub):
    result = await hub.exec.core.string.replace(
        data="", to_replace="test", replace_with="test_updated"
    )
    assert result["result"], result["ret"]
    assert "" == result["ret"]["data"]


@pytest.mark.asyncio
async def test_invalid_string_replace(hub):
    result = await hub.exec.core.string.replace(
        data=["test_data"], to_replace="test", replace_with="test_updated"
    )
    assert not result["result"], not result["ret"]
    assert "data for replace should be in string format" in result["comment"]
