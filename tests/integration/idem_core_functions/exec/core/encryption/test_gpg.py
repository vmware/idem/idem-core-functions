import gnupg
import pgpy
import pytest
from pgpy.constants import CompressionAlgorithm
from pgpy.constants import HashAlgorithm
from pgpy.constants import KeyFlags
from pgpy.constants import PubKeyAlgorithm
from pgpy.constants import SymmetricKeyAlgorithm


@pytest.fixture()
def keys(passphrase):
    gpg = gnupg.GPG()
    input_data = gpg.gen_key_input(
        name_email="keys@test", passphrase=passphrase, no_protection=passphrase is None
    )
    key = gpg.gen_key(input_data)
    private_keys = gpg.export_keys(
        key.fingerprint,
        True,
        passphrase=passphrase,
        expect_passphrase=passphrase is not None,
    )
    public_keys = gpg.export_keys(
        key.fingerprint,
        False,
        passphrase=passphrase,
        expect_passphrase=passphrase is not None,
    )
    # delete all of them, so they are not stored in default location and core function stay vanilla
    _delete_private_keys_in_cache(gpg, passphrase)
    yield key.fingerprint, private_keys, public_keys
    # delete them again to clean up after the test
    _delete_private_keys_in_cache(gpg, passphrase)
    _delete_public_keys_in_cache(gpg, passphrase)


@pytest.fixture()
def pgpy_keys(key_passphrase):
    key = pgpy.PGPKey.new(PubKeyAlgorithm.RSAEncryptOrSign, 4096)
    uid = pgpy.PGPUID.new("keys test", comment="keys test", email="keys@test")
    key.add_uid(
        uid,
        usage={KeyFlags.Sign, KeyFlags.EncryptCommunications, KeyFlags.EncryptStorage},
        hashes=[
            HashAlgorithm.SHA256,
            HashAlgorithm.SHA384,
            HashAlgorithm.SHA512,
            HashAlgorithm.SHA224,
        ],
        ciphers=[
            SymmetricKeyAlgorithm.AES256,
            SymmetricKeyAlgorithm.AES192,
            SymmetricKeyAlgorithm.AES128,
        ],
        compression=[
            CompressionAlgorithm.ZLIB,
            CompressionAlgorithm.BZ2,
            CompressionAlgorithm.ZIP,
            CompressionAlgorithm.Uncompressed,
        ],
    )

    if key_passphrase is not None:
        key.protect(key_passphrase, SymmetricKeyAlgorithm.AES256, HashAlgorithm.SHA256)

    yield key
    gpg = gnupg.GPG()
    # delete them again to clean up after the test
    _delete_private_keys_in_cache(gpg, key_passphrase)
    _delete_public_keys_in_cache(gpg, key_passphrase)


@pytest.mark.asyncio
async def test_gpg_encrypt_empty_data(hub):
    result = await hub.exec.core.encryption.gpg.encrypt(data="", recipients="invalid")

    assert not result["result"]
    assert "data for gpg_encrypt is empty" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_encrypt_failure(hub):
    data = "This is a encryption test"
    result = await hub.exec.core.encryption.gpg.encrypt(data=data, recipients="invalid")

    assert not result["result"]
    assert "invalid recipient" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_encrypt_success(hub):
    data = "This is a encryption test"
    result = await hub.exec.core.encryption.gpg.encrypt(
        data=data, symmetric=True, passphrase="test"
    )

    assert result["result"]
    assert "encryption ok" in str(result["comment"])
    assert result["ret"]
    # The encrypted data is different everytime
    assert result["ret"]["data"]


@pytest.mark.asyncio
async def test_gpg_decrypt_empty_message(hub):
    result = await hub.exec.core.encryption.gpg.decrypt("")

    assert not result["result"]
    assert "message for gpg_decrypt is empty" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_decrypt_failure(hub):
    data = "This is for decryption test"
    # cannot use as fixture as it is cached and no easy way to clear the cache
    gpg = gnupg.GPG()
    response = gpg.encrypt(data=data, recipients=[], passphrase="test", symmetric=True)

    # not providing passphrase - so expecting it to fail
    result = await hub.exec.core.encryption.gpg.decrypt(
        str(response.data.decode("utf-8"))
    )

    assert not result["result"]
    assert "decryption failed" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_decrypt_success(hub):
    data = "This is for decryption test"
    # cannot use as fixture as it is cached and no easy way to clear the cache
    gpg = gnupg.GPG()
    response = gpg.encrypt(data=data, recipients=[], passphrase="test", symmetric=True)
    result = await hub.exec.core.encryption.gpg.decrypt(
        message=str(response.data.decode("utf-8")), passphrase="test"
    )

    assert result["result"]
    assert "decryption ok" in str(result["comment"])
    assert result["ret"]
    assert "This is for decryption test" == result["ret"]["data"]


@pytest.mark.asyncio
@pytest.mark.parametrize("passphrase", (None, "test_passphrase"))
async def test_gpg_encrypt_with_recipient_key_decrypt_fails_when_recipient_does_not_use_corresponding_private_key(
    hub, passphrase, keys
):
    data = "This is for decryption test"
    fingerprint, private_keys, _ = keys

    # encrypt with recipient's key
    encrypt_result = await hub.exec.core.encryption.gpg.encrypt(
        data=data, recipients=fingerprint
    )

    assert encrypt_result["result"]
    assert "encryption ok" in str(encrypt_result["comment"])

    # Now recipient decrypts without private key -> it should fail
    decrypt_result = await hub.exec.core.encryption.gpg.decrypt(
        message=encrypt_result["ret"]["data"], passphrase=passphrase
    )
    assert not decrypt_result["result"]
    assert "no secret key" in str(decrypt_result["comment"])


@pytest.mark.asyncio
@pytest.mark.parametrize("passphrase", (None, "test_passphrase"))
async def test_gpg_encrypt_with_recipient_key_decrypt_succeeds_when_recipient_uses_corresponding_private_key(
    hub, keys, passphrase
):
    data = "This is for decryption test"
    fingerprint, private_keys, _ = keys

    # encrypt with recipient's key
    encrypt_result = await hub.exec.core.encryption.gpg.encrypt(
        data=data, recipients=fingerprint
    )

    assert encrypt_result["result"]
    assert "encryption ok" in str(encrypt_result["comment"])

    # Now recipient decrypts with his/her private key
    decrypt_result = await hub.exec.core.encryption.gpg.decrypt(
        message=encrypt_result["ret"]["data"],
        passphrase=passphrase,
        private_key=private_keys,
    )
    print(decrypt_result)
    assert decrypt_result["result"]
    assert "decryption ok" in str(decrypt_result["comment"])
    assert "This is for decryption test" == decrypt_result["ret"]["data"]


@pytest.mark.asyncio
@pytest.mark.parametrize("passphrase", (None, "test_passphrase"))
async def test_gpg_encrypt_decrypt_with_keys_generated_from_gpg(hub, keys, passphrase):
    data = "This is for decryption test"
    fingerprint, private_keys, public_keys = keys

    # encrypt with recipient's key
    encrypt_result = await hub.exec.core.encryption.gpg.encrypt(
        data=data, recipients=fingerprint, public_key=public_keys
    )

    assert encrypt_result["result"]
    assert "encryption ok" in str(encrypt_result["comment"])

    # Now recipient decrypts with his/her private key
    decrypt_result = await hub.exec.core.encryption.gpg.decrypt(
        message=encrypt_result["ret"]["data"],
        passphrase=passphrase,
        private_key=private_keys,
    )
    assert decrypt_result["result"]
    assert "decryption ok" in str(decrypt_result["comment"])
    assert "This is for decryption test" == decrypt_result["ret"]["data"]


@pytest.mark.asyncio
@pytest.mark.parametrize("key_passphrase", (None, "test_passphrase"))
async def test_gpg_encrypt_decrypt_with_keys_generated_from_pgpy(
    hub, key_passphrase, pgpy_keys
):
    data = "This is for decryption test"

    # encrypt with recipient's key
    # The fingerprint has spaces in the middle
    # This is something that needs to be handled by the consumer, if using pgpy.
    encrypt_result = await hub.exec.core.encryption.gpg.encrypt(
        data=data,
        recipients=pgpy_keys.pubkey.fingerprint.replace(" ", ""),
        public_key=str(pgpy_keys.pubkey),
    )

    assert encrypt_result["result"]

    # Now recipient decrypts with his/her private key
    decrypt_result = await hub.exec.core.encryption.gpg.decrypt(
        message=encrypt_result["ret"]["data"],
        private_key=str(pgpy_keys),
        passphrase=key_passphrase,
    )
    assert decrypt_result["result"]
    assert "decryption ok" in str(decrypt_result["comment"])
    assert "This is for decryption test" == decrypt_result["ret"]["data"]


@pytest.mark.asyncio
@pytest.mark.parametrize("key_passphrase", (None, "test_passphrase"))
async def test_pgpy_encrypt_gpg_decrypt(hub, key_passphrase, pgpy_keys):
    """
    This is to test secrets encrypted from non-gnupg tools
    """
    data = "This is for decryption test"

    loaded_pgp_key, _ = pgpy.PGPKey.from_blob(bytes(pgpy_keys.pubkey))
    message = pgpy.PGPMessage.new(
        data,
        compression=pgpy.constants.CompressionAlgorithm.Uncompressed,
    )
    encrypted_message = loaded_pgp_key.encrypt(message)

    # Now recipient decrypts with his/her private key
    decrypt_result = await hub.exec.core.encryption.gpg.decrypt(
        message=str(encrypted_message),
        private_key=str(pgpy_keys),
        passphrase=key_passphrase,
    )
    assert decrypt_result["result"]
    assert "decryption ok" in str(decrypt_result["comment"])
    assert "This is for decryption test" == decrypt_result["ret"]["data"]


def _delete_private_keys_in_cache(gpg, passphrase):
    # delete all of them, so they are not stored in default location
    all_private_keys = gpg.list_keys(secret=True)
    for key in all_private_keys.key_map.keys():
        try:
            deleted_result = gpg.delete_keys(
                key, secret=True, passphrase="" if passphrase is None else passphrase
            )
            print(
                f"Deleted private key result for the key {key}: {deleted_result.status}"
            )
        except Exception:
            pass


def _delete_public_keys_in_cache(gpg, passphrase):
    all_public_keys = gpg.list_keys()
    for key in all_public_keys.key_map.keys():
        try:
            deleted_result = gpg.delete_keys(
                key, secret=False, passphrase="" if passphrase is None else passphrase
            )
            print(
                f"Deleted public key result for the key {key}: {deleted_result.status}"
            )
        except Exception:
            pass
