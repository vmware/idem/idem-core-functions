import pytest


@pytest.mark.asyncio
async def test_base64encoding(hub):
    data = "test-user-name:test-password"
    result = await hub.exec.core.encoding.base64encode(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert "dGVzdC11c2VyLW5hbWU6dGVzdC1wYXNzd29yZA==" == ret.get("data")


async def test_base64encoding_fail_case(hub):
    data = ""
    result = await hub.exec.core.encoding.base64encode(data=data)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for base64encode is empty" in result["comment"]

    data = None
    result = await hub.exec.core.encoding.base64encode(data=data)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for base64encode is empty" in result["comment"]


@pytest.mark.asyncio
async def test_base64decoding(hub):
    encoded_data = "dGVzdC11c2VyLW5hbWU6dGVzdC1wYXNzd29yZA=="
    result = await hub.exec.core.encoding.base64decode(encoded_data=encoded_data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert "test-user-name:test-password" == ret.get("data")


@pytest.mark.asyncio
async def test_base64decoding_fail_case(hub):
    result = await hub.exec.core.encoding.base64decode(encoded_data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "encoded_data for base64decode is empty" in result["comment"]

    result = await hub.exec.core.encoding.base64decode(encoded_data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "encoded_data for base64decode is empty" in result["comment"]
